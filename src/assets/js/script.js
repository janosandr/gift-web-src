function addColors(numberOfColors, groups) {
    var blocks = document.querySelectorAll('.category-card'),
        colors = groups.reduce(function(arr, n) {
          var x = Math.floor(Math.random() * numberOfColors);
          while(arr.indexOf(x) >= 0) { // make sure colors are different
            x = (x + 1) % numberOfColors
          }
          return arr.concat(Array(n).fill(x));
        }, []);
    Array.from(blocks).forEach(function(block, idx) {
      block.classList.add('color' + colors[idx])
    })
  }
  
  document.addEventListener('DOMContentLoaded', function() {
    addColors(7, [4, 3, 2]);
  })

  	$(document).ready(function() {
			$('.minus').click(function () {
				var $input = $(this).parent().find('input');
				var count = parseInt($input.val()) - 1;
				count = count < 1 ? 1 : count;
				$input.val(count);
				$input.change();
				return false;
			});
			$('.plus').click(function () {
				var $input = $(this).parent().find('input');
				$input.val(parseInt($input.val()) + 1);
				$input.change();
				return false;
			});
		});