import Vue from "vue";
// import "./plugins/axios";
import VueRouter from "vue-router";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./routes";
import store from "./store";
import Clipboard from 'v-clipboard'
import VModal from 'vue-js-modal'
import VueToast from 'vue-toast-notification';
import titleMixin from './mixins/titleMixin'
import VueCountdown from '@chenfengyuan/vue-countdown';
import VueFuse from 'vue-fuse'

Vue.config.productionTip = false;

Vue.use(VueFuse)
Vue.use(VueToast);
Vue.use(VueRouter);
Vue.use(VModal);
Vue.use(Clipboard);
Vue.component(VueCountdown.name, VueCountdown);
Vue.mixin(titleMixin)

new Vue({
  router,
  store,
  // axios,
  render: h => h(App)
}).$mount("#app");