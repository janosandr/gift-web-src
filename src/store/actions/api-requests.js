import axios from "axios";

const token = 'c8a510143163b87b0030f5617e0e2622'

export default {
  // GET_PRODUCTS_FROM_API({commit}) {
  //   return axios('https://api.giftmarket.io/products', {
  //     method: "GET"
  //   })
  //     .then((products) => {
  //       commit('SET_PRODUCTS_TO_STATE', products.data.data);
  //       return products;
  //     })
  //     .catch((error) => {
  //       // console.log(error)
  //       return error;
  //     })
  // },
  GET_PRODUCTS_FROM_API({commit}) {
    return axios('https://api.giftmarket.io/v1/products?token=' + token, {
      method: "GET"
    })
      .then((products) => {
        commit('SET_PRODUCTS_TO_STATE', products.data.data);
        return products;
      })
      .catch((error) => {
        // console.log(error)
        return error;
      })
  },
  GET_BIP_PRICE({commit}){
    return axios.get('https://api.bip.dev/api/price')
    .then((bipPrice) => {
      commit('SET_BIP_PRICE', bipPrice)
      return bipPrice
    })
  },
  GET_RUB_PRICE({commit}){
    return axios.get('https://api.exchangeratesapi.io/latest?base=USD&symbols=RUB')
    .then((rubPrice) => {
      commit('SET_RUB_PRICE', rubPrice)
      return rubPrice
    })
  },
  GET_CATEGORIES_FROM_API({commit}) {
    return axios('https://api.giftmarket.io/v1/categories?token=' + token, {
      method: "GET"
    })
    .then((categories) => {
      commit('SET_CATEGORIES_TO_STATE', categories.data.data);
      return categories;
    })
    .catch((error) => {
      return error;
    })
  },
  GET_SYS_PRICE({commit}) {
    return axios.get('https://api.giftmarket.io/v1?token=' + token,)
    .then((sysPrice) => {
      commit('SET_SYS_PRICE', sysPrice.data)
      return sysPrice
    })
    .catch((error) => {

      return error;
    })
  }
}