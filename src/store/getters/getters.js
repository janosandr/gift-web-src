export default  {
    SEARCH_VALUE(state) {
      return state.searchValue;
    },
    IS_MOBILE(state) {
      return state.isMobile;
    },
    IS_DESKTOP(state) {
      return state.isDesktop;
    },
    PRODUCTS(state) {
      return state.products;
    },
    PRODUCT_ID: (state) => (id) => {
      return state.products.find(product => product.id == id)
    },
    CATEGORIES(state){
        return state.categories;
    },
    BIP_PRICE(state){
      return state.bipPrice
    },
    SYS_PRICE(state){
      return state.sysPrice
    },
    RUB_PRICE(state){
      return state.rubPrice
    },
    CATEGORY: (state) => (id) => {
        return state.categories.find(category => category.id == id)
    },
    PRODUCT: (state) => (id) => {
        return state.products.find(product => product.id == id)
    },
    CART(state) {
      return state.cart;
    }
  }
  