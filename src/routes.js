import VueRouter from "vue-router";
import Main from "./pages/Main";
import Product from "./pages/Product";
import Category from "./pages/Category";
import Categories from "./pages/Categories";
import Products from "./pages/Products";
import Merchant from "./pages/Merchant";
import Page from "./pages/Page";
import About from "./pages/About";

export default new VueRouter({
    routes: [{
            path: "/",
            title: "Главная",
            component: Main,
            name: "Main",
            props: true
        }, {
            path: "/about",
            title: "О нас",
            component: About,
            name: "О нас",
            props: true
        },

        {
            path: "/product/:id",
            title: "Сертификат",
            component: Product,
            name: "Product",
            props: true
        },
        {
            path: "/category/:id",
            title: "Категория",
            component: Category,
            name: "Category",
            props: true
        },
        {
            path: "/categories",
            title: "Категории",
            component: Categories,
            name: "Categories",
            props: true
        },
        {
            path: "/products",
            title: "Все сертификаты",
            component: Products,
            name: "Products",
            props: true
        },
        {
            path: "/merchant",
            title: "Оплата",
            component: Merchant,
            name: "Merchant",
            props: true
        },
        {
            path: "/agreement",
            component: Page,
            props: true
        }

    ],
    mode: "history"
});